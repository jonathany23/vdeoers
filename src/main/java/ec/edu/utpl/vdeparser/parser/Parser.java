/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdeparser.parser;

import ec.edu.utpl.util.ObjectJson;
import ec.edu.utpl.util.ObjectSigma;
import ec.edu.utpl.vde.exceptions.ApplicationException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Jonathan
 */
public class Parser<T> {
    
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    
    public T parse(String json, String graph) throws Exception{
        ObjectJson objJS = new ObjectJson();
        ObjectSigma objSigma = new ObjectSigma();
        ObjectMapper objMapper = new ObjectMapper();
        ObjectJson objJson = objMapper.readValue(json, ObjectJson.class);
        List<Map<Object, Object>> nodes = new ArrayList<Map<Object, Object>>();
        List<Map<Object, Object>> edges = new ArrayList<Map<Object, Object>>();
        URL path=null;
        if (graph.equals("1"))
            //path = "C:\\Users\\Jonathan\\Dropbox\\UTPL\\materias\\Tesis\\App\\VDEParser\\src\\main\\java\\ec\\edu\\utpl\\vdeparser\\d3\\equivalenciasD3.json";
            path = classLoader.getResource("../json/equivalencias.json");
        else if (graph.equals("sigma"))
            path = classLoader.getResource("../json/equivalenciasSigma.json");
        else if (graph.equals("3"))
            //path = "C:\\Users\\Jonathan\\Dropbox\\UTPL\\materias\\Tesis\\App\\VDEParser\\src\\main\\java\\ec\\edu\\utpl\\vdeparser\\d3\\equivalenciasD3.json";
            path = classLoader.getResource("../json/equivalencias.json");
        else if (graph.equals("4"))
            //path = "C:\\Users\\jonathan\\Dropbox\\UTPL\\materias\\Tesis\\App\\VDEScopus\\src\\main\\java\\ec\\edu\\utpl\\vdeparser\\parser\\equivalenciasReddit.json";
            path = classLoader.getResource("../json/equivalenciasReddit.json");
        else if (graph.equals("5"))
            path = classLoader.getResource("../json/equivalencias.json");
        
        if (path != null) {
            for (Object map : objJson.getNodes()) {
                nodes.add(replacePredicate(map,path));
            }
        } else 
            System.out.println("Codigo de Grafica no definido o Incorrecto");
        
        System.out.println("List: "+nodes.size());
        if (graph.equals("1") || graph.equals("5")) {
            objJS.setNodes(nodes);
            objJS.setLinks(objJson.getLinks());
            return (T) objJS;
        } else if (graph.equals("sigma")) {
            objSigma.setNodes(nodes);
            for (Object map : objJson.getLinks()) {
                edges.add(parseLinks(map));
            }
            objSigma.setEdges(edges);
            return (T) objSigma;
        } else if (graph.equals("3")){
            System.out.println("############################ 3333333333");
            List<Map<Object, Object>> listLinks = new ArrayList<Map<Object, Object>>();
            for (Object map : objJson.getLinks()) {
                listLinks.add(parseLinks(map));
            }
            
            int target=0, source, cont=0;
            //List<Map<Object, Object>> litChild = new ArrayList<Map<Object, Object>>();
            Map<Object, Object> mapChild = new HashMap<Object, Object>();
            Map<Integer, Map<Object, Object>> mapAux = new HashMap<Integer, Map<Object, Object>>();
            Map<Integer, Map<Object, Object>> mapAux2 = new HashMap<Integer, Map<Object, Object>>();
            List<Map<Object, Object>> listMap = new ArrayList<Map<Object, Object>>();
            for (Map<Object, Object> map : listLinks) {
                if (map.get("target")!= null && map.get("source")!=null) {
                    target= Integer.parseInt(map.get("target").toString());
                    source = Integer.parseInt(map.get("source").toString());
                    
                    if (!mapAux.containsKey(source))
                        mapAux.put(source, nodes.get(source));
                    if (!mapAux.containsKey(target))
                        mapAux.put(target, nodes.get(target));
                }                 
//                    //if (mapChild.isEmpty()) {
//                        //mapChild = nodes.get(idxNodo);
//                    //}
//                    //mapChild.put("children", nodes.get(source));
////                    litChild.add(mapChild);
            }
            
            Map<Integer, Integer> mapNumberNodes = countNodesRelation(listLinks);
            for (Map<Object, Object> map : listLinks) {
                target = Integer.parseInt(String.valueOf(map.get("target")));
                source = Integer.parseInt(String.valueOf(map.get("source")));
                System.out.println("source: "+source+" / target: "+target);
                if (mapNumberNodes.get(target) >= mapNumberNodes.get(source))
                    mapAux.get(target).putAll(addChild(mapAux, target, source));
                else if (mapNumberNodes.get(target) < mapNumberNodes.get(source))
                    mapAux.get(source).putAll(addChild(mapAux, source, target));
//                int max = Collections.max(mapNumberNodes.values());
                int max = getMaxFromMap(mapNumberNodes);
                System.out.println("max: "+max);
                if (target == max || source == max) {
                    listMap = null;
                    listMap = new ArrayList<Map<Object, Object>>();
                    listMap.add(mapAux.get(max));
                }
            }
            
            
            /*for (int i = listLinks.size(); i > 0; i--) {
                //source = Integer.parseInt(listLinks.get(i-1).get("source").toString());
                target = Integer.parseInt(listLinks.get(i-1).get("target").toString());
                //mapAux.get(target).put("children", mapAux.get(source));
                listMap = new ArrayList<Map<Object, Object>>();
                for (int j = listLinks.size(); j > 0; j--) {
                    if (target == Integer.parseInt(listLinks.get(j-1).get("target").toString())) {
                        source = Integer.parseInt(listLinks.get(j-1).get("source").toString());
                        listMap.add(mapAux.get(source));
                    }
                }
                mapAux.get(target).put("children", listMap);
                mapAux2.put(target, mapAux.get(target));
            }
            System.out.println("FIN ############################ 3333333333");
//            return (T) litChild;
            List<Map<Object, Object>> listF = new ArrayList<Map<Object, Object>>();
            listF.add(mapAux2.get(target));
            return (T) listF;*/
            
            //Se comenta debido a que no se usa
            //listMap = replaceIds(listMap, 0);
            
            return (T) listMap;
        } else if (graph.equals("4")) {
            objJS.setNodes(nodes);
            
            //Map<String, Double> weight = new HashMap<String, Double>();
            Random rnd = new Random();
            //weight.put("weight", rnd.nextDouble());
            //objJson.getLinks().add(weight);
            ObjectMapper m = new ObjectMapper();
            List<Map<Object, Object>> lstMap = new ArrayList<Map<Object, Object>>();
            Map<Object, Object> as;// = m.convertValue(objLinks, Map.class);
            //as = null;
            for (Object objLinks : objJson.getLinks()) {
                as = m.convertValue(objLinks, Map.class);
                as.put("weight", rnd.nextDouble());
                lstMap.add(as);
            }
//            for (Object map : objJson.getLinks()) {
//                System.out.println("sds: "+map);
//            }
            //objJS.setLinks(objJson.getLinks());
            objJS.setLinks(lstMap);
            return (T) objJS;
        }
        return null;
    }
    
    /***
     * Remplaza las etiquetas del Json a devolver 
     * dependiendo con lo parametrizado en el archivo de equivalencias que recive como parametro
     * @param value
     * @param path
     * @return
     * @throws FileNotFoundException
     * @throws JSONException 
     */
    private Map<Object, Object> replacePredicate(Object value, URL path) throws FileNotFoundException, JSONException {
        //String path = "C:\\Users\\Jonathan\\Dropbox\\UTPL\\materias\\Tesis\\App\\VDEParser\\src\\main\\java\\ec\\edu\\utpl\\vdeparser\\d3\\equivalenciasD3.json";
        InputStream is = new FileInputStream(path.getPath());
        String json = getStringFromInputStream(is);
        JSONObject jsonObject = new JSONObject(json);
        JSONArray arr = jsonObject.names();
        String val;
        boolean isNames = false;
        
        ObjectMapper m = new ObjectMapper();
        Map<Object, Object> as = m.convertValue(value, Map.class);
        
        Map<Object, Object> predicates = new HashMap<Object, Object>();
        for (Map.Entry<Object, Object> entry : as.entrySet()) {
            for (int i = 0; i < arr.length(); i++) {
                if (entry.getKey().equals(arr.getString(i))) {
                    isNames = true;
                    val = jsonObject.get(arr.getString(i)).toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\\"", "");
                    if (!val.isEmpty()) {
                        predicates.put(val, entry.getValue());
                    } else {
                        predicates.put(entry.getKey(), entry.getValue());
                    }
                    break;
                }
            }
            if (!isNames) {
                predicates.put(entry.getKey(), entry.getValue());
            }
            isNames = false;
        }
        
        return predicates;
    }
    
    private static String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
     }

    /***
     * Arma un Mapa con las relaciones obtenidas del primer Json, 
     * se lo realiza aqui devido a que el obtenido original los datos estan como Objects
     * @param map
     * @return 
     */
    public Map<Object, Object> parseLinks(Object map) {
        ObjectMapper m = new ObjectMapper();
        Map<Object, Object> as = m.convertValue(map, Map.class);
        
        for (Map.Entry<Object, Object> entry : as.entrySet()) {
            if (entry.getKey().equals("source")) {
                entry.setValue(entry.getValue().toString());
            } else if (entry.getKey().equals("target")) {
                entry.setValue(entry.getValue().toString());
            }
        }
        return as;
    }

    /***
     * Crea un mapa con el id de todos los nodos del Json como claves y el numero de ocurrencias en el Json como valor
     * @param listLinks
     * @return 
     */
    public Map<Integer, Integer> countNodesRelation(List<Map<Object, Object>> listLinks) {
        Map<Integer, Integer> numberNodes = Collections.emptyMap();
        if (listLinks!=null) {
            numberNodes = new TreeMap<Integer, Integer>();
            Integer source, target;
            for (Map<Object, Object> map : listLinks) {
                target = Integer.parseInt(String.valueOf(map.get("target")));
                source = Integer.parseInt(String.valueOf(map.get("source")));
                if (numberNodes.containsKey(target))
                    numberNodes.put(target, numberNodes.get(target)+1);
                else
                    numberNodes.put(target, 1);
                
                if (numberNodes.containsKey(source))
                    numberNodes.put(source, numberNodes.get(source)+1);
                else
                    numberNodes.put(source, 1);
            }
        }else
            throw new ApplicationException("Lista de Relaciones Recivida esta vacia o null.");
            //throw new IllegalArgumentException("[Parser](countNodesRelation) Lista de Relaciones Recivida esta vacia o null");
        return numberNodes;
    }

    /***
     * Arma la estructura del Json tipo Tree, Agrega los elementos hijos de un nodo
     * @param Node
     * @param max
     * @param min
     * @return 
     */
    public Map<? extends Object, ? extends Object> addChild(Map<Integer, Map<Object, Object>> Node, int max, int min) {
        if (Node != null && Node.size() > 0) {
            if (Node.get(max).containsKey("children")) {
                ((List) Node.get(max).get("children")).add(Node.get(min));
            } else {
                List<Object> listTemp = new ArrayList<Object>();
                listTemp.add(Node.get(min));
                Node.get(max).put("children", listTemp);
                System.out.println("children" + Node.get(min).get("children"));
                listTemp = null;
            }
        }
        return  Node.get(max);
    }

    /***
     * Devuelve la clave del mapa con mayor nuemro de ocurrencias en relaciones del primer Json obtenido
     * @param map
     * @return 
     */
    public int getMaxFromMap(Map<Integer, Integer> map) {
        int compare=0;
        int val =0;
        if (map!= null && map.size()>0) {
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                if (entry.getValue().compareTo(compare)>0) {
                    compare = entry.getValue();
                    val = entry.getKey();
                }
            }
        } else
            throw new IllegalArgumentException("[Parser](getMaxFromMap) Mapa Recivido esta vacio o null");
        return val;
    }

    
    /***
     * Replaza los IDs originales por IDs secuenciales, evitando la distribucion de nodos en desorden
     * @param listMap
     * @param cont
     * @return 
     */
    //Se Comenta devido a que no se usa
    /*public List<Map<Object, Object>> replaceIds(List<Map<Object, Object>> listMap, int cont) {
        if (listMap!=null) {
            for (Map<Object, Object> map : listMap) {
                if (map.containsKey("id")){
                    map.put("id", String.valueOf(cont));
                    cont++;
                }
                if (map.containsKey("children")) {
                    if (map.get("children") instanceof List) {
                        replaceIds((List<Map<Object, Object>>)map.get("children"), cont);
                        cont++;
                    }
                }
            }
        }else
            throw new ApplicationException("No se ha podido restablecer lod IDs debiado a que el parametro esta vacio");
        return listMap;
    }*/
}
