/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.model;

/**
 *
 * @author jonathan
 */
public class Publication extends Basic{
    private String date;
    private String section;
    private String volume;
    private String keyWords;
    private String description;

    public Publication(String date, String section, String volume, String keyWords, String description, String name, int group) {
        this.date = date;
        this.section = section;
        this.volume = volume;
        this.keyWords = keyWords;
        this.description = description;
        this.setName(name);
        this.setGroup(group);
    }
    
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
