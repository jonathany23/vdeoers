/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.dao;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.Model;
import ec.edu.utpl.util.ExecQuery;
import ec.edu.utpl.util.OperationsVirtuosoJson;
import ec.edu.utpl.util.QuerysSparql;
import ec.edu.utpl.util.ReadCSV;
import ec.edu.utpl.util.ReplaceParameters;
import ec.edu.utpl.util.StringUtil;
import ec.edu.utpl.util.VDEConstant;
import ec.edu.utpl.vde.exceptions.ApplicationException;
import ec.edu.utpl.vdescopusserver.model.Country;
import ec.edu.utpl.vdescopusserver.model.DataMap;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtModel;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 *
 * @author jonathan
 */
public class CountrysDao {
    
    public static VirtuosoQueryExecution vur;
    public static final String graph = VDEConstant.VIRT_GRAPH;
    static String url;
    static String user;
    static String password;
    
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    URL path = classLoader.getResource(VDEConstant.PATH_QUERYS_SPARQL);
    QuerysSparql qs = new QuerysSparql();
    OperationsVirtuosoJson opVirt = new OperationsVirtuosoJson();
    ReplaceParameters rp = new ReplaceParameters();
    ExecQuery eq = new ExecQuery();
    StringUtil su = new StringUtil();
    
    public List<Map<String, String>> getCountries() throws Exception{
        DataMap dm = new DataMap();
        getDBCountries(dm);
        return dm.getCountrys();
    }
    
    public List<Map<String, String>> getCities() throws SQLException, JSONException{
        DataMap dm = new DataMap();
        getDBCities(dm);
        return dm.getCountrys();
    }
    
    public List<String> getInstitutes(String country, String city) throws SQLException{
        Country co = new Country();
        getDBInstitutes(co, country, city);
        return co.getCountrys();
    }

    private void getDBCountries(DataMap co) throws Exception {
//        VirtGraph set;
//        try {
//            set = new VirtGraph (VDEConstant.VIRT_SERVER, VDEConstant.VIRT_USER, VDEConstant.VIRT_PASS);
//            url = VDEConstant.VIRT_SERVER;
//            user = VDEConstant.VIRT_USER;
//            password = VDEConstant.VIRT_PASS;
//            set.clear();
//        } catch (Exception e) {
//            throw new ApplicationException("Error Conectarse a Virtuoso, Por favor revise su configuracion e intente nuevamente.");
//        }
//        Model model = VirtModel.openDatabaseModel(graph, url, user, password);
        String sprql = qs.getQuerySparlFromCSVFile(path, "3");
        
        System.out.println("sprql: "+sprql);
        if (sprql != null) {
//            VirtuosoQueryExecution vqm = (VirtuosoQueryExecution) VirtuosoQueryExecutionFactory.create(sprql, model);
//            com.hp.hpl.jena.query.ResultSet results = vqm.execSelect();
            String json = eq.query(sprql, true); 
            if (json != null && !json.isEmpty()) {
                List<JSONObject> listJsObj = opVirt.getBindingsObjects(json);
                JSONArray arrVars = opVirt.getHeadVars(json);
                String type, value;
                Map<String, String> mapVal = null;
                for (JSONObject jSONObject : listJsObj) {
                    mapVal = new HashMap<String, String>();
                    for (int i = 0; i < arrVars.length(); i++) {
                        type = opVirt.getBindingsObjectsVars(jSONObject, arrVars.getString(i)).getString("type");
                        value = opVirt.getBindingsObjectsVars(jSONObject, arrVars.getString(i)).getString("value");
                        if ("uri".equals(type) && value.contains("http")) {
                            String data = getLabelName("<"+value+">");
                            if (data!=null && !data.trim().isEmpty()) {
                                mapVal.put("key", value);
                                mapVal.put("value", data);
                                co.setCountrys(mapVal);
                            }
                        }
                        
                    }
                }
            }
            
//            //Obtener los elemnetos de la etiqueta response
//            JSONObject arrJson = (JSONObject) jsObj.get("results");
//            String value = "";
//            
//            //Obtener los elemnetos de la etiqueta response->docs
//            JSONArray jsOERs = arrJson.getJSONArray("bindings");
//            for (int i = 0; i < jsOERs.length(); i++) {
//                //System.out.println("DATA: "+jsOERs.get(i));
//                JSONObject varObj = (JSONObject) jsOERs.get(i);
//                JSONObject varO = (JSONObject) varObj.get("o");
//                //System.out.println(varO.get("value"));
//                String val;
//                val = String.valueOf(varO.get("value"));
//                if (val.contains("#"))
//                    val = getLast(val, "#");
//                if (val.contains("/"))
//                    val = getLast(val, "/");
//                co.setCountrys(val);
//            }
////            String pre = null;
////            while (results.hasNext()) {
////                QuerySolution result = results.nextSolution();
////                pre = result.get("o").toString();
////                co.setCountrys(pre);
////            }
        }
    }
    
    public String getLabelName(String value) throws JSONException{
        if ((value != null && !value.trim().isEmpty())) {
            System.out.println("//value: "+value);
            String sparql = qs.getQuerySparlFromCSVFile(path, "4");
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("1", value);
            sparql = rp.replaceParameters(sparql, parameters);
            if (sparql!=null && !sparql.trim().isEmpty()) {
                String json = eq.query(sparql, false);
                if (json != null && !json.trim().isEmpty()) {
                    List<JSONObject> objJs = opVirt.getBindingsObjects(json);
                    for (JSONObject jSONObject : objJs) {
                        return opVirt.getBindingsObjectsVars(jSONObject, "institution").getString("value");
                    }
                }
            }
        }
        return null;
    }
    
    //comento por que se cre una clase especifica para esto (StringUtil)
//    public String getLast (String text, String patron){
//        if (text!=null && patron != null) {
//            int pos;
//            pos = text.lastIndexOf(patron);
//            text = text.substring(pos+1, text.length());
//        }
//        return text;
//    }
    
    private void getDBCities(DataMap co) throws SQLException, JSONException {
//        VirtGraph set;
//        set = new VirtGraph(VDEConstant.VIRT_SERVER, VDEConstant.VIRT_USER, VDEConstant.VIRT_PASS);
//        url = VDEConstant.VIRT_SERVER;
//        user = VDEConstant.VIRT_USER;
//        password = VDEConstant.VIRT_PASS;
//        set.clear();

        String sprql = qs.getQuerySparlFromCSVFile(path, "1");
        
        System.out.println("sprql: "+sprql);
        if (sprql != null && !sprql.trim().isEmpty()) {
            
            String json = eq.query(sprql, true);
            List<JSONObject> jsObj = opVirt.getBindingsObjects(json);
            Map<String, String> mapPar;
            for (JSONObject jSONObject : jsObj) {
                mapPar = new HashMap<String, String>();
                String label = opVirt.getBindingsObjectsVars(jSONObject, "label").getString("value");
                String value = label;
                if (value.contains("#"))
                    value = su.getLastChar(value, "#");
                if (label.contains("/"))
                    value = su.getLastChar(value, "/");
                mapPar.put("key", label);
                mapPar.put("value", value);
                co.setCountrys(mapPar);
            }
//            Model model = VirtModel.openDatabaseModel(graph, url, user, password);
//            VirtuosoQueryExecution vqm = (VirtuosoQueryExecution) VirtuosoQueryExecutionFactory.create(sprql, model);
//            com.hp.hpl.jena.query.ResultSet results = vqm.execSelect();
            

//            while (results.hasNext()) {
//                QuerySolution result = results.nextSolution();
//                String cities[] = result.get("o").toString().split("\\^");
//                if (cities[0] != null) {
//                    co.setCountrys(cities[0]);
//                }
//            }
        }
    }
    
    private void getDBInstitutes(Country co, String country, String city) throws SQLException {
        VirtGraph set;
            set = new VirtGraph (VDEConstant.VIRT_SERVER, VDEConstant.VIRT_USER, VDEConstant.VIRT_PASS);
            url = VDEConstant.VIRT_SERVER;
            user = VDEConstant.VIRT_USER;
            password = VDEConstant.VIRT_PASS;
            set.clear();
            
        String sprql = qs.getQuerySparlFromCSVFile(path, "3");
        
        System.out.println("sprql: " + sprql);
        if (sprql != null && !sprql.trim().isEmpty()) {
            Map<String, String> parameters = new LinkedHashMap<String, String>();
            parameters.put("1", country);
            parameters.put("2", city);
            ReplaceParameters rp = new ReplaceParameters();
            sprql = rp.replaceParameters(sprql, parameters);

            Model model = VirtModel.openDatabaseModel(graph, url, user, password);
            VirtuosoQueryExecution vqm = (VirtuosoQueryExecution) VirtuosoQueryExecutionFactory.create(sprql, model);
            com.hp.hpl.jena.query.ResultSet results = vqm.execSelect();

            while (results.hasNext()) {
                QuerySolution result = results.nextSolution();
                String cities[] = result.get("o").toString().split("\\^");
                if (cities[0] != null) {
                    co.setCountrys(cities[0]);
                }
            }
        }
    }
    
    public static Connection getConnection(String uriconnection){
        Connection conexion = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String jdbcutf8 = "&useUnicode=true&characterEncoding=UTF-8";
            conexion = DriverManager.getConnection(uriconnection+jdbcutf8);
            
        } catch (ClassNotFoundException ex) {
            System.out.println("error conexion1 :"+ex.getCause());
        } catch (SQLException ex){
            System.out.println("error conexion2 :"+ex.getCause());
        }
        return conexion;

    }
}
