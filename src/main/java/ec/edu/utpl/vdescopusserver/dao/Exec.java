/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.dao;

import ec.edu.utpl.vdescopusserver.model.Author;
import ec.edu.utpl.vdescopusserver.model.ForceDirected;
import ec.edu.utpl.vdescopusserver.model.Institution;
import ec.edu.utpl.vdescopusserver.model.Links;
import ec.edu.utpl.vdescopusserver.model.Publication;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Jonathan
 */
public class Exec {
    public ForceDirected execute (String country, String city, String institution) throws SQLException{
        TreeMap<String, Integer> mapAll = new TreeMap<String, Integer>();
        InstitutionDao inst = new InstitutionDao();
        List<Institution> listInst = new ArrayList<Institution>();
        listInst = inst.institution(country, city, institution);
        mapAll.putAll(inst.mapInst);
        
        AuthorsDao auth = new AuthorsDao();
        List<Author> listAuth = new ArrayList<Author>();
        listAuth = auth.authors(country, city, institution,mapAll.size());
        mapAll.putAll(auth.mapAuthors);
        
        PublicationDao pubD = new PublicationDao();
        List<Publication> listPublic = new ArrayList<Publication>();
        System.out.println("Last Key: "+auth.mapAuthors.lastKey());
        listPublic = pubD.publications(country, city, institution, mapAll.size());
        mapAll.putAll(pubD.mapPub);
        
        
//        Iterator it = mapAll.entrySet().iterator();
//
//        while (it.hasNext()) {
//            Map.Entry e = (Map.Entry) it.next();
//            System.out.println("[" + e.getKey() + "=" + e.getValue() + "]");
//        }
        for (Map.Entry<String, Integer> entry : mapAll.entrySet()) {
//            String string = entry.getKey();
//            Integer integer = entry.getValue();
            System.out.println("[" + entry.getKey() + "=" + entry.getValue() + "]");
        }
        
        LinksDao ld = new LinksDao();
        List<Links> listLinks = new ArrayList<Links>();
        listLinks = ld.relacionar(country, city, institution, inst.mapInst, auth.mapAuthors, pubD.mapPub);
        
        ForceDirected fd;
        fd = new ForceDirected();
        for (Institution insti : listInst) {
            fd.setNodes(insti);
        }
        for (Author author : listAuth) {
            fd.setNodes(author);
        }
        for (Publication publication : listPublic) {
            fd.setNodes(publication);
        }
        fd.setLinks(listLinks);
        return fd;
    }
}
