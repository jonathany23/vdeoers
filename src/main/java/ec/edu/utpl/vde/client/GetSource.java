/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vde.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import ec.edu.utpl.util.VDEConstant;
import javax.ws.rs.core.MultivaluedMap;

/**
 * Jersey REST client generated for REST resource:GenericResource [generic]<br>
 * USAGE:
 * <pre>
 *        GetSource client = new GetSource();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author jonathan
 */
public class GetSource {
    private WebResource webResource;
    private Client client;
    private static final String BASE_URI = VDEConstant.WS_BASE_URI;//"http://localhost:8080/VDEScopus/webresources";

    public GetSource() {
        com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();
        client = Client.create(config);
        webResource = client.resource(BASE_URI).path("vde");
    }

    public void putJson(Object requestEntity) throws UniformInterfaceException {
        webResource.type(javax.ws.rs.core.MediaType.APPLICATION_JSON).put(requestEntity);
    }

//    public <T> T getPub4Inst(Class<T> responseType, String country, String city, String institute) throws UniformInterfaceException {
//        WebResource resource = webResource;
//        resource = resource.path(java.text.MessageFormat.format("{0}/{1}/{2}", new Object[]{country, city, institute}));
//        return resource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
//    }
    public String getPub4Inst(String country, String city, String institute) throws UniformInterfaceException {
        WebResource resource = webResource;
        resource = resource.path(java.text.MessageFormat.format("{0}/{1}/{2}", new Object[]{country, city, institute}));
        return resource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(ClientResponse.class).getEntity(String.class);
    }
    
    public String getCountries() throws UniformInterfaceException {
        WebResource resource = webResource;
        resource = resource.path("countries");//.path(java.text.MessageFormat.format("{0}", new Object[]{path}));
        System.out.println("resource: "+resource);
        return resource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(ClientResponse.class).getEntity(String.class);
    }

    public ClientResponse remove(String id) throws UniformInterfaceException {
        return webResource.path(java.text.MessageFormat.format("{0}", new Object[]{id})).delete(ClientResponse.class);
    }

    public <T> T getInstitute(Class<T> responseType, String country, String city) throws UniformInterfaceException {
        WebResource resource = webResource;
        resource = resource.path(java.text.MessageFormat.format("{0}/{1}", new Object[]{country, city}));
        return resource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public <T> T getJson(Class<T> responseType) throws UniformInterfaceException {
        WebResource resource = webResource;
        return resource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

//    public <T> T getQueryJson(Class<T> responseType) throws UniformInterfaceException {
//        WebResource resource = webResource;
//        resource = resource.path("params");
//        return resource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
//    }
    public String getQueryJson(MultivaluedMap<String, String> mapParm) throws UniformInterfaceException {
        WebResource resource = webResource;
        resource = resource.path("params").queryParams(mapParm);
        System.out.println(resource.getURI().toString());
        return resource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(ClientResponse.class).getEntity(String.class);
    }

    public <T> T getCitiesPost(Class<T> responseType, Object requestEntity, String country) throws UniformInterfaceException {
        return webResource.path(java.text.MessageFormat.format("{0}", new Object[]{country})).type(javax.ws.rs.core.MediaType.TEXT_PLAIN).post(responseType, requestEntity);
    }

    public <T> T getCities(Class<T> responseType, String country) throws UniformInterfaceException {
        WebResource resource = webResource;
        resource = resource.path(java.text.MessageFormat.format("{0}", new Object[]{country}));
        return resource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void close() {
        client.destroy();
    }
    
}
