/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vde.exceptions;

import com.sun.jersey.api.Responses;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jonathan
 */
public class ApplicationException extends WebApplicationException {

    public ApplicationException() {
        super(Responses.notFound().build());
    }

    public ApplicationException(String message) {
        super(Response.status(Responses.NOT_FOUND).entity(message).type(MediaType.APPLICATION_JSON).build());
    }
    
    
}
