/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.util;

import java.net.URL;
import java.util.List;

/**
 *
 * @author jonathan
 */
public class QuerysSparql {
    
    public String getQuerySparlFromCSVFile(URL pathCSV, String queryNumber){
        if ((pathCSV != null && !pathCSV.getPath().trim().isEmpty())
                && (queryNumber != null && !queryNumber.trim().isEmpty())) {
            
            ReadCSV rCSV = new ReadCSV();
            List<String[]> itemsCsv = rCSV.readCVS(pathCSV.getPath());
            for (String[] arr : itemsCsv) {
                if (arr.length >= 2) {
                    if (queryNumber.equals(arr[0])) {
                        if (arr[1] != null && !arr[1].trim().isEmpty()) {
                            return arr[1];
                        }
                        break;
                    }
                }
            }
        }
        return null;
    }
}
