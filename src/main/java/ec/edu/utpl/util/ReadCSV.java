/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author jonathan
 */
public class ReadCSV {
    
    public List<String []> readCVS(String path){
        //List querys = Collections.EMPTY_LIST;
        List<String[]> querys = new ArrayList<String[]>();
        BufferedReader br = null;
        String line = null;
        
        if ((path != null && !path.trim().isEmpty())) {
            try {
                br = new BufferedReader(new FileReader(path));
                while((line = br.readLine()) != null){
                    String[] lineItems = line.split(VDEConstant.CSV_SPLITER);
                    querys.add(lineItems);
                }
            } catch (FileNotFoundException fnfe) {
                fnfe.printStackTrace();
            } catch (IOException ioe){
                ioe.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return querys;
    }
    
}
