/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.util;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import java.net.URLEncoder;

/**
 *
 * @author jonathan
 */
public class ExecQuery {
    
    public String query (String query, boolean graph){       
         try {
            String salida = "&output=json";
            String url = "";
            query = URLEncoder.encode(query, "UTF-8");
            
            Client client = new Client();
             if (graph)
                 url = VDEConstant.VIRT_END_POINT + "?default-graph-uri=" + VDEConstant.VIRT_GRAPH
                    + "&query=" + query + salida;
             else
                 url = VDEConstant.VIRT_END_POINT + "?default-graph-uri=&query=" + query + salida;
             System.out.println(url);
            WebResource resource = client.resource(url);
            String result = resource.accept("application/sparql-results+json").get(String.class);

            result = result.replaceAll("\n", "");
            //return result;
             System.out.println(result);
             return result;
        } catch (Exception ex) {
            System.err.println(ex.getCause());
            //return "";
        }
         return null;
    }
}
