/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.util;

import java.util.Map;

/**
 *
 * @author jonathan
 */
public class ReplaceParameters {
    
    public String replaceParameters(String query, Map<String, String> parameters){
        if (query != null && !query.trim().isEmpty() && parameters != null) {
            for (Map.Entry<String, String> mapParameters : parameters.entrySet()) {
                String key = mapParameters.getKey();
                String value = mapParameters.getValue();
                System.out.println("K: "+key+" / V: "+value);
                if (value!=null && !value.trim().isEmpty()) {
                    String search = "@" + key;
                    String replace = value;
                    if (query.contains(search)) {
                        query = query.replaceAll(search, replace);
                    }
                }
            }
        }
        System.out.println(query);
        return query;
    }
}
