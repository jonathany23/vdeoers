/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.util;

//import com.hp.hpl.jena.sparql.lib.org.json.JSONArray;
//import com.hp.hpl.jena.sparql.lib.org.json.JSONException;
//import com.hp.hpl.jena.sparql.lib.org.json.JSONObject;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jonathan
 */
public class OperationsVirtuosoJson {
    
    public JSONObject getHead(String json) throws JSONException{
        JSONObject head = null;
        if (json != null && json.length()!=0) {
            JSONObject jsObj = new JSONObject(json);
            head = jsObj.getJSONObject("head");
        }
        return head;
    }
    
    public JSONArray getHeadLinks(String json) throws JSONException{
        JSONArray links = null;
        if (json != null && json.length()!=0) {
            JSONObject head = getHead(json);
            links = head.getJSONArray("link");
        }
        return links;
    }
    
    public JSONArray getHeadVars(String json) throws JSONException{
        JSONArray vars = null;
        if (json != null && json.length()!=0) {
            JSONObject head = getHead(json);
            vars = head.getJSONArray("vars");
        }
        return vars;
    }
    
    public JSONObject getResults(String json) throws JSONException{
        JSONObject results = null;
        if (json != null && json.length()!=0) {
            JSONObject jsObj = new JSONObject(json);
            results = (JSONObject) jsObj.get("results");
        }
        return results;
    }
    
    public JSONArray getResultsBindings(String json) throws JSONException{
        JSONArray bindings = null;
        if (json != null && json.length()!=0) {
            JSONObject results = getResults(json);
            bindings = results.getJSONArray("bindings");
        }
        return bindings;
    }
    
    public List<JSONObject> getBindingsObjects(String json) throws JSONException{
        JSONObject objects = null;
        List<JSONObject> listJson = new ArrayList<JSONObject>();
        if (json != null && json.length()!=0) {
            JSONArray bindings = getResultsBindings(json);
            for (int i = 0; i < bindings.length(); i++) {
                objects = new JSONObject();
                objects = bindings.getJSONObject(i);
                listJson.add(objects);
            }
        }
        return listJson;
    }
    
    public JSONObject getBindingsObjectsVars(JSONObject jsonObj, String var) throws JSONException{
        JSONObject objVar = null;
        if ((jsonObj != null) && (var!=null && var.length()!=0)) {
            objVar = jsonObj.getJSONObject(var);
        }
        return objVar;
    }
}
