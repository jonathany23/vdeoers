/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *
 * @author jonathan
 */
public class StringUtil {
    
    public String getFirstChar(String string, String split) {
        String val = "";
        if ((string != null && string.length() != 0) && (split != null && split.length() != 0)) {
            int ic = 0;
            ic = string.indexOf(split);
            val = string.substring(0, ic);
        }
        return val;
    }
    
    public String getLastChar(String string, String split) {
        String val = "";
        if ((string != null && string.length() != 0) && (split != null && split.length() != 0)) {
            int ic = 0;
            ic = string.lastIndexOf(split);
            val = string.substring(ic + 1, string.length());
        }
        return val;
    }
    
    public String getStringFromInputStream(InputStream is) {
        StringBuilder sb = new StringBuilder();
        if (is != null) {
            BufferedReader br = null;
            String line;
            try {
                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return String.valueOf(sb);
    }
}
