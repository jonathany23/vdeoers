/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.util;

/**
 *
 * @author jonathan
 */
public class VDEConstant {
    //public static final String VIRT_GRAPH = "http://www.utpl.edu.ec/test/oers";
    public static final String VIRT_SERVER = "jdbc:virtuoso://127.0.0.1:1111/charset=UTF-8/";//"jdbc:virtuoso://apolo.utpl.edu.ec:1111/charset=UTF-8/";
    public static final String VIRT_USER = "dba";
    public static final String VIRT_PASS = "dba";//"dbaljtpl121";
    
    public static final String VIRT_END_POINT = "http://data.utpl.edu.ec/serendipity/oar/sparql";
    public static final String VIRT_GRAPH = "http://data.utpl.edu.ec/serendipity/oar";
    
    public static final String WS_BASE_URI = "http://localhost:8080/vdeoers/webresources";//"http://carbono.utpl.edu.ec:8080/VDEScopus-1.0-SNAPSHOT/webresources";
    
    public static final String PATH_QUERYS_SPARQL = "../csv/querys.csv";
    public static final String CSV_SPLITER = "%";
}
