/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.util;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 *
 * @author jonathan
 */
public class CreateTempJson {

    public File createFile(URL pathTemp) {
        try {

            //create a temp file
            System.out.println("pathTemp.toString(): "+pathTemp.getPath());
            File temp = File.createTempFile("data", ".json", new File(pathTemp.getPath()));
            System.out.println("Temp file : " + temp.getAbsolutePath());
            return temp;
        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }
}
