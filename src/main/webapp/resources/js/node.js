/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {

    $.ajax({
        type: "GET",
        url: "http://localhost:8080/VDEScopus/webresources/vde",
        dataType: "json",
        success: function(data) {
            // var c = document.getElementById(id);
            // data.forEach(function(names) {
            //     var t = document.createElement("option");
            //     t.value = names;
            //     t.textContent = names;
            //     c.appendChild(t);
            loadFieldsNames('cmb_find');
                //var countries = #{newJerseyClient.json};
                var s = document.getElementById('countries');

                data.forEach(function(country) {
                    var t = document.createElement("option");
                    t.value = country;
                    t.textContent = country;
                    s.appendChild(t);
                }); 
                $(".input_fields_wrap").hide();
            //});
        }
    });


    var max_fields = 10; //maximum input boxes allowed
    var wrapper = $(".input_fields_wrap_add"); //Fields wrapper
    var add_button = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e) { //on add input button click
        e.preventDefault();
        if (x < max_fields) { //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="fields-wrap"><select name="find'+x+'" size="1" id="cmb_find'+x+'" class="cmb_find"></select><input type="text" name="find'+x+'" class="input-find" id="input_find'+x+'"/><a href="#" class="remove_field">Borrar</a></div>'); //add input box
            loadFieldsNames("cmb_find"+x);
        }
    });

    $(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    })

    //########################################
    var chage = false;
    $("#sh-search").click(function() {
        if (chage==false){
          $(".input_fields_wrap").show(1000);  
          chage=true;
        } else{
            $(".input_fields_wrap").hide(1000);  
            chage=false;
        }
    });
    
    $(".searchServer").click(function(){
        var path;
        var p;
        var so;
        //var country, city, institute;
        
        path="?graph=1&country="+strCountry+"&city="+strCity+"&name="+strInstitute;
        $(".input-find").each(function() {
            so = $(this).val();
            if (so!=null && so.length!=0){
                path+="&";
                var c = document.getElementsByName($(this).attr("name"));
                p = $(c).val();
                path+=p+"="+$(this).val();
            }
        });
        console.log(path);
        loadData(path);
    });
});